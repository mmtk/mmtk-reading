# MMTk Reading Group

This repo contains papers we'll be reading for the MMTk reading group.

## Meetings

We will meet at 3pm Thursday AEDT each week.


Date|Paper(s)
 --- | --- 
[Thu 7/12/17](https://www.timeanddate.com/worldclock/fixedtime.html?msg=MMTk+Reading&iso=20171207T15&p1=57&ah=1) | _Oil and Water? High Performance Garbage Collection in Java with MMTk_ ([paper](https://gitlab.anu.edu.au/mmtk/mmtk-reading/tree/master/papers/mmtk-icse-2004.pdf), [talk](https://gitlab.anu.edu.au/mmtk/mmtk-reading/tree/master/papers/mmtk-icse-2004-talk.pdf)), _Myths and Realities: The Performance Impact of Garbage Collection_ ([paper](https://gitlab.anu.edu.au/mmtk/mmtk-reading/tree/master/papers/mmtk-sigmetrics-2004.pdf), [talk](https://gitlab.anu.edu.au/mmtk/mmtk-reading/tree/master/papers/mmtk-sigmetrics-2004-talk.pdf))
[Thu 14/12/17](https://www.timeanddate.com/worldclock/fixedtime.html?msg=MMTk+Reading&iso=20171221T15&p1=57&ah=1) | _Immix: A Mark-Region Garbage Collector with Space Efficiency, Fast Collection, and Mutator Performance_ ([paper](https://gitlab.anu.edu.au/mmtk/mmtk-reading/tree/master/papers/immix-pldi-2008.pdf), [talk](https://gitlab.anu.edu.au/mmtk/mmtk-reading/tree/master/papers/immix-pldi-2008-talk.pdf)), _Taking Off the Gloves with Reference Counting Immix_ ([paper](https://gitlab.anu.edu.au/mmtk/mmtk-reading/tree/master/papers/rcix-oopsla-2013.pdf), [talk](https://gitlab.anu.edu.au/mmtk/mmtk-reading/tree/master/papers/rcix-oopsla-2013-talk.pdf))
[Thu 21/12/17](https://www.timeanddate.com/worldclock/fixedtime.html?msg=MMTk+Reading&iso=20171214T15&p1=57&ah=1) | _Fast, Multicore-Scalable, Low-Fragmentation Memory Allocation through Large Virtual Memory and Global Data Structures_ ([paper](http://www.cs.uni-salzburg.at/~ck/content/publications/conferences/OOPSLA15-Scalloc.pdf), [talk](http://www.cs.uni-salzburg.at/~ck/content/talks/NETYS15-Scalloc.pdf))
[Thu 4/1/18](https://www.timeanddate.com/worldclock/fixedtime.html?msg=MMTk+Reading&iso=20180104T15&p1=57&ah=1) | _Barriers Reconsidered, Friendlier Still!_ ([paper](https://gitlab.anu.edu.au/mmtk/mmtk-reading/tree/master/papers/barrier-ismm-2012.pdf)), _A Comprehensive Evaluation of Object Scanning Techniques_ ([paper](https://gitlab.anu.edu.au/mmtk/mmtk-reading/tree/master/papers/scan-ismm-2011.pdf))
Thu 18/1/18 | Steve away
[Thu 25/1/18](https://www.timeanddate.com/worldclock/fixedtime.html?msg=MMTk+Reading&iso=20180125T15&p1=57&ah=1) | _Effective Prefetch for Mark-Sweep Garbage Collection_ ([paper](https://gitlab.anu.edu.au/mmtk/mmtk-reading/tree/master/papers/pf-ismm-2007.pdf)), _Why Nothing Matters: The Impact of Zeroing_ ([paper](https://gitlab.anu.edu.au/mmtk/mmtk-reading/tree/master/papers/zero-oopsla-2011.pdf))



